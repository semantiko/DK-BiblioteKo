This repository is composed of the K standard library's translation into Dedukti.

The K standard library is available here: https://kframework.org/k-distribution/include/kframework/builtin/

The hierarchy of the translation files follows that of the standard K library, i.e.:
  - **domains**: Basic datatypes which are universally useful.
       * Default Modules
       * Arrays
       * Maps
       * Sets
       * Lists
       * Collection Conversions
       * Booleans
       * Integers
       * IEEE 754 Floating-point Numbers
       * Strings
       * String Buffers
       * Byte Arrays
       * Program identifiers
       * Equality and conditionals
       * Meta operations
       * I/O in K
       * Machine Integers
       * Strategies

  - **kast**: Representation of K internal data-structures (not to be included in normal definitions).
       -> Translatable?
  - **prelude**: Automatically included into every K definition.
       -> prelude.md = kast.md + domains.md.
  - **ffi**: The K Foreign Function Interface (FFI) module provides a way to call native functions directly from a K semantics using the C ABI.
       -> Translatable?
  - **json**: K provides builtin support for reading/writing to JSON.
       -> Translatable?
  - **rat**: K provides support for arbitrary-precision rational numbers represented as a quotient between two integers. 
       -> Not yet implemented.
  - **substitution**: Hooked implementation of capture-aware sustitution for K definitions.
       -> Not yet implemented.
  - **unification**: Hooked implementation of unification exposed directly to K definitions.
       -> Translatable?
